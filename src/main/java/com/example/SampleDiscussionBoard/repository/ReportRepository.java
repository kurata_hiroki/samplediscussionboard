package com.example.SampleDiscussionBoard.repository;


import com.example.SampleDiscussionBoard.entity.Report;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ReportRepository extends JpaRepository<Report, Integer> {
}