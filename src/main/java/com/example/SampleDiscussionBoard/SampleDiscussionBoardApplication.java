package com.example.SampleDiscussionBoard;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SampleDiscussionBoardApplication {

	public static void main(String[] args) {
		SpringApplication.run(SampleDiscussionBoardApplication.class, args);
	}

}
